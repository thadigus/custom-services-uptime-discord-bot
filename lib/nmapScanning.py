import nmap3, socket

def nmap_ping_scan(ip, name):
    nmap = nmap3.NmapScanTechniques()
    nmap_results = nmap.nmap_ping_scan(str(ip))
    nmap_svc_port_status = nmap_results[ip]["state"]["state"]
    if nmap_svc_port_status == "up":
        nmap_server_pingable = "ONLINE"
        nmap_server_status = ":white_check_mark: "
        try:
            nmap_svc_dns = nmap_results[ip]["hostname"][0]["name"] + " "
        except:
            nmap_svc_dns = offline_dns_lookup(ip)
        server_message = nmap_server_status + "Server " + name + " on server " + nmap_svc_dns + "(" + str(ip) +  ") reports as " + nmap_server_pingable + "\n\n"
    else:
        nmap_server_pingable = "down"
        nmap_server_status = ":octagonal_sign: "
        nmap_svc_dns = offline_dns_lookup(ip)
        server_message = nmap_server_status + "Server " + name + " on server " + nmap_svc_dns + "(" + str(ip) +  ") reports as " + nmap_server_pingable + "!" + "\n\n"
    return server_message

def nmap_service_scan(ip, port, name):
    nmap = nmap3.Nmap()
    nmap_results = nmap.nmap_version_detection(str(ip), args="-p " + str(port))
    nmap_svc_port_status = nmap_results[ip]["state"]["state"]
    if nmap_svc_port_status == "up":
        if nmap_results[ip]["ports"][0]["state"] == "open":
            nmap_server_pingable = "ONLINE"
            nmap_server_status = ":white_check_mark: "
            try:
                nmap_svc_dns = nmap_results[ip]["hostname"][0]["name"] + " "
            except:
                nmap_svc_dns = offline_dns_lookup(ip)
            try:
                nmap_svc_message = nmap_results[ip]["ports"][0]["service"]["extrainfo"]
            except:
                nmap_svc_message = ""
            server_message = nmap_server_status + "Service " + name + " on server " + nmap_svc_dns + str(ip) + ":" + str(port) + " reports as " + nmap_server_pingable + "\n" + nmap_svc_message + "\n\n"
    else:
        nmap_server_pingable = "down"
        nmap_server_status = ":octagonal_sign: "
        nmap_svc_dns = offline_dns_lookup(ip)
        server_message = nmap_server_status + "Service " + name + " on server " + nmap_svc_dns + str(ip) + ":" + str(port) + " reports as " + nmap_server_pingable + "\n\n"
    return server_message

def offline_dns_lookup(ip):
    try:
        dns_name = socket.getfqdn(ip)
    except:
        dns_name = ""
    return dns_name