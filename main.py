import discord
from discord.ext import tasks
from lib.configurationimport import load_config_params
from lib.nmapScanning import nmap_ping_scan, nmap_service_scan

# Library initalizations
intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)
tree = discord.app_commands.CommandTree(client)
config = load_config_params("config.yml")

# Bot ping command to test connection
@tree.command(name = "ping", description = "Test operability of Bot.")
async def minecraft_uptime(interaction):
    await interaction.response.send_message("pong")

# Startup Message to Display Uptime
async def channel_setup():
    channel = client.get_channel(config["CHANNEL_ID"])
    await channel.purge()
    botmsg = await channel.send("UptimeBot is Initializing...")
    return botmsg

# Startup Message to Display Uptime
@tasks.loop(seconds=10)
async def uptime_stats(botmsg):
    server_message = ""
    for server in config["PING_CHECK_SERVERS"]:
        scan_out = ""
        try:
            scan_out = nmap_ping_scan(server["ip"], server["name"])
        except KeyboardInterrupt:
            exit()
        except:
           print("Failed to perform service scan on: " + server["name"])
        server_message += scan_out
    for service in config["SERVICES"]:
        scan_out = ""
        try:
            scan_out = nmap_service_scan(service["ip"], service["port"], service["name"])
        except KeyboardInterrupt:
            exit()
        except:
            print("Failed to perform ping scan on: " + service["name"])
        server_message += scan_out
    await client.wait_until_ready()
    if server_message != "":
        await botmsg.edit(content=server_message)
    else:
        await botmsg.edit(content="Bot could not parse config.yml")

# On_ready performed when bot connects for the first time
@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')
    await tree.sync()
    botmsg = await channel_setup()
    uptime_stats.start(botmsg)

# Starts bot
client.run(config["DISCORD_TOKEN"])