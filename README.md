# Custom Services Uptime Discord Bot

A containerized service tracker to display service availability in a Discord server.

![pipeline status badge](https://gitlab.com/thadigus/custom-services-uptime-discord-bot/badges/master/pipeline.svg)

## Installation

Register at the [Discord Developer Portal](https://discord.com/developers/applications) and create a new application.

Fill out details on application to your own desire.

Add a bot in the Bot tab and reset token to get the Discord token required for operation.

Enable all privileged gateway intents on Bot page.

Go to OAuth2 -> URL Generator tab. Enable the `bot` scope and then add all permissions you feel comfortable adding for this API key. Then copy the url at the bottom and visit it with your authenticated Discord account in your browser to add the bot to your server.

Enable developer mode in your Discord client and [get the channel ID](https://turbofuture.com/internet/Discord-Channel-ID) for the dedicated channel to display the uptime dashboard.

Download and build the bot.

```bash
git clone git@gitlab.com:thadigus/custom-services-uptime-discord-bot.git`
cd custom-services-uptime-discord-bot
cp example-config.yml config.yml
vi config.yml
```

Open config.yml and add Discord key for the bot and the channel ID for the channel to dedicate to the bot (all content will be deleted). Add services and admins as shown in [example](#usage).

### Running with Python (Debugging)

Below are the commands to setup and run the bot with Python. This is great for debugging the config.yml file and testing the bot in general. You can do this to ensure that everything is working as intended before submitting an issue, and to ensure that your config.yml file is working as intended before deploying to production.

```bash
sudo apt update
sudo apt install nmap -y
pip install -r requirements.txt
python main.py
```

### Running in Docker Container (Recommended)

The intended deployment of this bot is as a Docker container due to its non-persistent and daemon-like nature. We highly recommend using our Dockerfile to build the container once you have verified that your config.yml file works properly. **Ensure that you are using your lab DNS server if you have one** so you can pull you local DNS names. If you do not run a DNS server this option can be omitted from the `docker run` step.

```bash
docker build -t discord-uptime-bot .
docker run -d --dns="8.8.8.8" --name discord-uptime-bot discord-uptime-bot
docker ps
```

## Usage

The config.yml file is the only necessary editing for this bot. You can place your token, channel ID, Admin usernames (coming soon), and servers/services to keep track of as shown below. You can add as many servers/services as you would like by simply creating more YAML compliant entries. Ping check servers only require a name for the check and an IP address and they will keep track of servers with a ping check. Services require a specific port to monitor as well and they will be monitored using `nmap` for service enumeration. Service scans only require a SYN ACK to be considered online so application level monitoring must be performed elsewhere.

**Coming Soon* you will be able to add a list of admin usernames for the bot to direct message in the event that a server goes down. Admins will be able to acknowledge the alert. Acknowledgement will be added to the entry to indicate to users that the problem is known by administrators. The service will only resolve to ONLINE once the bot sees the service online again.

Config.yml Example

```yaml
---
DISCORD_TOKEN: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-C' # Discord API key for bot created in developer portal.
CHANNEL_ID: 12345678901234567890 # Channel ID of dedicated channel for bot to post current uptime.
ADMIN_USERNAME: # List of admins on the server to notify when services become unavailable
  - name: "YourUsername#1234" # Discord username of admin
PING_CHECK_SERVERS: # List of servers that should use a ping only check.
  - name: server_ping # Unique name to identify specific ping scan.
    ip: 0.0.0.0 # IP address or hostname of asset to ping scan
  - name: another_server_ping
    ip: 192.168.1.1
    port: 443
SERVICES: # List of services with their IP and ports that should be maintained.
  - name: service_ping # Unique name to identify specific service scan.
    ip: 0.0.0.0 # IP address or hostname of service to scan
    port: 443 # Port of service to scan
  - name: another_service_ping
    ip: 192.168.1.1
    port: 443
```

## Support

Please open an [issue](https://gitlab.com/thadigus/custom-services-uptime-discord-bot/-/issues/new) if you have any problems. I will prioritize and help as I can.

## Roadmap

Please create issues for feature requests or any ideas. I will prioritize them and add them if they are a good fit.

## Authors and acknowledgment

### [Thad Turner](https://thadigus.gitlab.io/) - Cybersecurity Practitioner

## License

There is no current license specification for this project.

## Project status

This project is not under steady development but I will add features and bugfixes as they come in or as they are requested by myself and others.
